﻿using UnityEngine;
[System.Serializable]
public struct WeaponValues
{
    public int ShootSpeed;    //Выстрелы в секунду
    public float WeaponDamage;
    public float ShootDistance;
    public Transform HitHole;
}
public class Weapon : MonoBehaviour {
    public Animator animController;
    protected bool isShooting, canUse;
    protected RaycastHit rayHit;
    public LayerMask hitMask;
    //Smooth motion
    public float FactorX { get; set; }
    public float FactorY { get; set; }
    float amount = 0.02f;
    float maxAmount = 0.03f;
    float smooth = 3f;
    private Vector3 def;
    //
    [HideInInspector]
    public bool Walk, Run, Crouch, Grounded;
    public void SetState(MoveMode moveMode)
    {
        switch (moveMode)
        {
            case MoveMode.Idle:
                animController.SetBool("Walk", false);
                animController.SetBool("Run", false);
                animController.SetBool("Grounded", true);
                FactorY = 0f;
                break;
            case MoveMode.Walk:
                animController.SetBool("Walk", true);
                animController.SetBool("Run", false);
                animController.SetBool("Grounded", true);
                break;
            case MoveMode.Run:
                animController.SetBool("Walk", true);
                animController.SetBool("Run", true);
                animController.SetBool("Grounded", true);
                break;
            case MoveMode.Falling:
                FactorY = Mathf.Lerp(FactorY, 0.07f, Time.deltaTime * 5f);
                animController.SetBool("Grounded", false);
                animController.SetBool("Walk", false);
                animController.SetBool("Run", false);
                break;
        }
    }
    public virtual void Deploy()
    {
        enabled = true;
        canUse = true;
    }
    public virtual void Attack1(Transform playerCamera)
    {
    }
    public virtual void Attack2(Transform playerCamera)
    {
    }
    protected virtual void Start()
    {
        def = transform.localPosition;
    }
    protected virtual void Update()
    {
        var factorX = Mathf.Clamp(FactorX - Input.GetAxis("Mouse X") * amount, -maxAmount, maxAmount);
        var factorY = Mathf.Clamp(FactorY - Input.GetAxis("Mouse Y") * amount, -maxAmount, maxAmount);
        Vector3 Final = new Vector3(def.x + factorX, def.y + factorY, def.z);
        transform.localPosition = Vector3.Lerp(transform.localPosition, Final, Time.deltaTime * smooth);
    }  
}