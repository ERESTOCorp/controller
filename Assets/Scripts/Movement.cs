﻿using UnityEngine;
[System.Serializable]
public struct PlayerValues
{
    public float WalkSpeed;
    public float RunSpeed;
    public float CrouchSpeed;
    public float JumpHeight;
}
public enum MoveMode
{
    Idle, Crouch, Walk, Run, Falling
}
public class Movement : MonoBehaviour {
    public bool IsGrounded { get; private set; }
    [SerializeField] PlayerValues Values;
    [SerializeField] LayerMask Ground;
    public MoveMode MoveMode
    {
        get; private set;
    }
    private Character AlignCharacter;
    private Vector3 moveVector;

    private float curSpeed;
    private Rigidbody body;
    private Transform bodyTransform;    //чтобы не вызывать неявно(через transform.) GetComponent<Transform>()
    private CapsuleCollider bodyCollider;

    private void Start()
    {
        body = GetComponent<Rigidbody>();
        bodyCollider = GetComponent<CapsuleCollider>();
        bodyTransform = transform;
    }
    private void Update()
    {
        IsGrounded = Physics.CheckSphere(new Vector3(transform.position.x, transform.position.y - bodyCollider.height * 0.4f, transform.position.z), bodyCollider.radius, Ground, QueryTriggerInteraction.Ignore);
        moveVector.x = Input.GetAxis("Horizontal");
        moveVector.z = Input.GetAxis("Vertical");
        if (IsGrounded)
        {
            if (Input.GetButton("Run"))
            {
                curSpeed = Values.RunSpeed;
                MoveMode = MoveMode.Run;
            }
            else if(Input.GetButton("Crouch"))
            {
                curSpeed = Values.CrouchSpeed;
                MoveMode = MoveMode.Crouch;
            }
            else
            {
                curSpeed = Values.WalkSpeed;
                if(moveVector != Vector3.zero)
                    MoveMode = MoveMode.Walk;
                else
                    MoveMode = MoveMode.Idle;
            }

            if (Input.GetButtonDown("Jump"))
            {
                body.AddForce(Vector3.up * Mathf.Sqrt(Values.JumpHeight * -2f * Physics.gravity.y), ForceMode.VelocityChange);
            }
        }
        else
        {
            MoveMode = MoveMode.Falling;
        }
    }
    private void FixedUpdate()
    {
        body.MovePosition(body.position + bodyTransform.TransformDirection(moveVector) * curSpeed * Time.fixedDeltaTime);
    }
}
