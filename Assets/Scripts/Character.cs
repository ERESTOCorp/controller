﻿using UnityEngine;
public class Character : MonoBehaviour {
    public Movement Movement { get; private set; }
    public Weapon ActiveWeapon
    {
        get
        {
            return activeWeapon;
        }
        private set
        {
            activeWeapon = value;
            activeWeapon.Deploy();
        }
    }
    private Weapon activeWeapon;
    public Weapon[] weapons;
    private Transform playerCamera;
    private void Start()
    {
        playerCamera = Camera.main.transform;
        Movement = GetComponent<Movement>();
        ActiveWeapon = weapons[0];
    }
    private void Update()
    {
        activeWeapon.SetState(Movement.MoveMode);
        if (Input.GetButton("Fire1"))
            activeWeapon.Attack1(playerCamera);
        if (Input.GetButton("Fire2"))
            activeWeapon.Attack2(playerCamera);
    }
}
