﻿using UnityEngine;
using System.Collections;
public class HID : Weapon
{
    public WeaponValues SMGValues, SniperValues;
    public bool IsSMG
    {
        get
        {
            return isSMG;
        }
        private set
        {
            isSMG = value;
            if (value == false)
                StartCoroutine(ChangeSMGToSniper());
            else
                StartCoroutine(ChangeSniperToSMG());
        }
    }
    private bool isSMG = false;

    protected override void Start()
    {
        base.Start();
    }
    protected override void Update()
    {
        base.Update();
        if (!canUse)
            return;
        if (Input.GetKeyDown(KeyCode.V))
        {
            IsSMG = !IsSMG;
        }
    }
    public override void Attack1(Transform playerCamera)
    {
        if (!canUse)
            return;
        if (isSMG)
        {
            if (!isShooting)
                StartCoroutine(SMGAttack1(playerCamera));
        }
        else
        {
            if (!isShooting)
                StartCoroutine(SniperAttack1(playerCamera));
        }
    }
    public override void Attack2(Transform playerCamera)
    {
        if (!canUse)
            return;
        if (isSMG)
        {
            StartCoroutine(SMGAttack2(playerCamera));
        }
        else
        {
            StartCoroutine(SniperAttack2(playerCamera));
        }
    }
    protected IEnumerator SniperAttack1(Transform playerCamera)
    {
        isShooting = true;
        if (Physics.Raycast(playerCamera.position, playerCamera.forward, out rayHit, SniperValues.ShootDistance, hitMask))
        {
            Instantiate(SniperValues.HitHole, rayHit.point, Quaternion.identity);
        }
        animController.Play("Fire1shot", 0, SniperValues.ShootSpeed);
        yield return new WaitForSeconds(1f / SniperValues.ShootSpeed);
        isShooting = false;
    }
    protected IEnumerator SniperAttack2(Transform playerCamera)
    {
        animController.Play("Aim0");
        yield return null;
    }

    protected IEnumerator SMGAttack1(Transform playerCamera)
    {
        isShooting = true;
        if (Physics.Raycast(playerCamera.position, playerCamera.forward, out rayHit, SMGValues.ShootDistance, hitMask))
        {
            Instantiate(SMGValues.HitHole, rayHit.point, Quaternion.identity);
        }
        animController.Play("SMGFire1shot", 0, SMGValues.ShootSpeed);
        yield return new WaitForSeconds(1f / SMGValues.ShootSpeed);
        isShooting = false;
    }
    protected IEnumerator SMGAttack2(Transform playerCamera)
    {
        animController.Play("Aim0");
        yield return null;
    }

    public IEnumerator ChangeSMGToSniper()
    {
        canUse = false;
        animController.Play("SMGToSniper");
        yield return new WaitForSeconds(1f);
        canUse = true;
    }
    public IEnumerator ChangeSniperToSMG()
    {
        canUse = false;
        animController.Play("SniperToSMG");
        yield return new WaitForSeconds(1f);
        canUse = true;
    }
}